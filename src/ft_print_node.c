/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_node.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:09:36 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

void		ft_print_node(t_listmax *node)
{
	int	tty;

	tty = ft_get_tty();
	if (node->is_select)
		ft_set_param("mr");
	if (node->is_cursed)
		ft_set_param("us");
	ft_putstr_fd(node->param, tty);
	ft_set_param("me");
	ft_putchar_fd('\n', tty);
}
