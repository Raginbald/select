/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:05:06 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

int	main(int argc, char **argv)
{
	t_listmax		*list;
	char			key[5];

	ft_set_term();
	list = ft_create_list_max(argc, argv);
	ft_print_list(list);
	while (ft_strcmp("\E", key) && list)
	{
		ft_bzero(key, 4);
		read(0, key, 4);
		ft_send_key(key, &list);
	}
	ft_exit(list);
	return (0);
}
