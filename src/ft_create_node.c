/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_node.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:38:26 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

t_listmax	*ft_create_node(char *param)
{
	t_listmax	*node;

	node = (t_listmax *)malloc(sizeof(t_listmax));
	if (!node)
	{
		ft_putstr_fd(MALLOC_ERROR, 2);
		ft_exit(NULL);
	}
	node->param = param;
	node->is_select = 0;
	node->is_cursed = 0;
	node->next = NULL;
	node->prev = NULL;
	return (node);
}

