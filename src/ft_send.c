/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_send.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:12:46 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

int		ft_count_nb(t_listmax **list)
{
	t_listmax	*first;
	int			i;

	i = 0;
	first = *list;
	if ((*list)->is_select)
		i++;
	(*list) = (*list)->next;
	while ((*list) != first)
	{
		if ((*list)->is_select)
			i++;
		(*list) = (*list)->next;
	}
	return (i);
}

void	ft_send(t_listmax **list)
{
	int	nb_select;
	int	i;

	nb_select = ft_count_nb(list);
	i = 0;
	while ((*list))
	{
		if ((*list)->is_select)
		{
			ft_putstr((*list)->param);
			if (i < nb_select - 1)
				ft_putchar(' ');
			i++;
		}
		ft_delete_node(list);
	}
	if (nb_select)
		ft_putchar('\n');
	ft_exit(NULL);
}
