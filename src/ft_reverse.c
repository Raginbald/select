/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reverse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:12:20 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

void	ft_reverse(t_listmax **list)
{
	t_listmax	*tmp;

	tmp = (*list)->next;
	(*list)->is_select = 1 - (*list)->is_select;
	while (tmp != *list)
	{
		tmp->is_select = 1 - tmp->is_select;
		tmp = tmp->next;
	}
}
