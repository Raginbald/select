/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_tty.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:10:11 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

int		ft_get_tty(void)
{
	static int	fd = 0;

	if (fd == 0)
		fd = open("/dev/tty", O_WRONLY);
	if (fd == -1)
	{
		ft_putstr_fd("tty error\n", 2);
		ft_exit(NULL);
	}
	return (fd);
}
