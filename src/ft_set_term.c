/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_set_term.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:07:46 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

void	ft_set_term(void)
{
	struct termios	term;
	char			*name_term;

	if ((name_term = getenv("TERM")) == NULL)
	{
		write(2, GETENV_ERROR, ft_strlen(GETENV_ERROR));
		ft_exit(NULL);
	}
	if (tgetent(NULL, name_term) == ERR)
	{
		write(2, TGETENT_ERROR, ft_strlen(TGETENT_ERROR));
		ft_exit(NULL);
	}
	if (tcgetattr(0, &term) == -1)
	{
		write(2, TCGETATTR_ERROR, ft_strlen(TCGETATTR_ERROR));
		ft_exit(NULL);
	}
	ft_get_backup();
	term.c_lflag &= ~(ICANON | ECHO | ISIG);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;
	ft_set_param("vi");
	tcsetattr(0, 0, &term);
}
