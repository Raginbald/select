/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_send_key.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:12:07 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

static t_list_operand	ope[] = {
	{"\E[A", &ft_up},
	{"\E[B", &ft_down},
	{" ", &ft_select},
	{"\177", &ft_delete},
	{"\E[3~", &ft_delete},
	{"\n", &ft_send},
	{"r", &ft_reverse},
	{{26, 0, 0, 0, 0}, &ft_pause},
	{"", NULL}
};

void	ft_send_key(char key[5], t_listmax **list)
{
	int	i;

	i = 0;
	while (ope[i].f)
	{
		if (ft_strcmp(ope[i].str, key) == 0)
		{
			ft_blank_screen(*list);
			ope[i].f(list);
			if (*list)
				ft_print_list(*list);
			return ;
		}
		i++;
	}
}
