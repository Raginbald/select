/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_set_param.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:09:50 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

void	ft_set_param(char *param)
{
	if (!param || !param[0] || !param[1] || param[2])
		return ;
	tputs(tgetstr(param, 0), 1, ft_putchar_tty);
}
