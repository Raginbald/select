/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_blank_screen.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:13:17 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

void	ft_blank_screen(t_listmax *list)
{
	t_listmax	*tmp;

	if (!list)
		return ;
	ft_set_param("up");
	ft_set_param("dl");
	tmp = list->next;
	while (tmp != list)
	{
		ft_set_param("up");
		ft_set_param("dl");
		tmp = tmp->next;
	}
}
