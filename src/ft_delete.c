/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:12:03 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

void	ft_delete(t_listmax **list)
{
	t_listmax	*tmp;

	tmp = *list;
	if ((*list)->next == *list)
	{
		ft_delete_node(list);
		*list = NULL;
	}
	else if ((*list)->is_cursed)
	{
		ft_delete_node(list);
		(*list)->is_cursed = 1;
	}
	else
	{
		while (!tmp->is_cursed)
			tmp = tmp->next;
		tmp->is_cursed = 0;
		tmp->next->is_cursed = 1;
		ft_delete_node(&tmp);
	}
}

