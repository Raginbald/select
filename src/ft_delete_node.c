/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_node.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:16:55 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

void	ft_delete_node(t_listmax **node)
{
	t_listmax	*father;
	t_listmax	*son;
	t_listmax	*tokill;

	tokill = *node;
	if (tokill->next == tokill)
		*node = NULL;
	else
	{
		father = tokill->prev;
		son = tokill->next;
		father->next = son;
		son->prev = father;
		*node = son;
	}
	free(tokill);
}
