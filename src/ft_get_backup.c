/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_backup.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:18:46 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

struct termios	*ft_get_backup(void)
{
	static struct termios	*backup = NULL;

	if (backup == NULL)
	{
		backup = (struct termios *)malloc(sizeof(struct termios));
		tcgetattr(0, backup);
	}
	return (backup);
}

void			ft_set_backup(void)
{
	tcsetattr(0, 0, ft_get_backup());
}
