/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_list_max.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:07:56 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "select.h"

t_listmax	*ft_create_list_max(int argc, char **argv)
{
	t_listmax	*root;
	t_listmax	*tmp;
	int			i;

	if (argc == 1)
	{
		ft_putstr_fd(USAGE_ERROR, 2);
		ft_exit(NULL);
	}
	i = 1;
	root = NULL;
	while (i < argc)
	{
		ft_append_node(&root, ft_create_node(argv[i]));
		i++;
	}
	tmp = root;
	while (tmp->next)
		tmp = tmp->next;
	root->prev = tmp;
	tmp->next = root;
	return (root);
}
