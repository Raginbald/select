/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   select.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/04 16:05:30 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/09 12:44:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SELECT_H
# define SELECT_H

# include <string.h>
# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <termios.h>
# include <fcntl.h>
# include <curses.h>
# include <term.h>
# include <signal.h>
# include "libft.h"

# define GETENV_ERROR "Error: Term name not found\n"
# define TGETENT_ERROR "Error: Terminfo database not found\n"
# define TCGETATTR_ERROR "Error while using tcgetattr\n"
# define MALLOC_ERROR "Error while using malloc\n"
# define USAGE_ERROR "usage: ./select param1...\n"

typedef struct			s_listmax{
	char				*param;
	int					is_select;
	int					is_cursed;
	struct s_listmax	*next;
	struct s_listmax	*prev;
}						t_listmax;

typedef struct			s_list_operand{
	char				str[5];
	void				(*f)(t_listmax **);
}						t_list_operand;

void				ft_delete_node(t_listmax **node);
void				ft_print_list(t_listmax *list);
void				ft_send_key(char key[5], t_listmax **list);
void				ft_set_term(void);
void				ft_set_backup(void);
void				ft_set_param(char *param);
void				ft_blank_screen(t_listmax *list);
void				ft_down(t_listmax **list);
void				ft_up(t_listmax **list);
void				ft_select(t_listmax **list);
void				ft_delete(t_listmax **list);
void				ft_send(t_listmax **list);
void				ft_reverse(t_listmax **list);
void				ft_pause(t_listmax **list);
void				ft_exit(t_listmax *list);
void				ft_erase_list(t_listmax *list);
void				ft_print_node(t_listmax *node);
void				ft_append_node(t_listmax **root, t_listmax *node);
int					ft_printf(char *, ...);
int					ft_putchar_tty(int c);
int					ft_get_tty(void);
t_listmax		*	ft_create_list_max(int argc, char **argv);
t_listmax		*	ft_create_node(char *param);
struct termios	*	ft_get_backup(void);

#endif
