# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/31 12:40:37 by graybaud          #+#    #+#              #
#    Updated: 2014/01/09 17:05:47 by graybaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_select

FLAGS = -Werror -Wextra -Wall -O3

SRCDIR = src

OBJDIR = obj

HEAD = includes

HEADLIBFT = libft/includes

LIBFTDIR = libft

SRC =	ft_append_node.c		\
		ft_blank_screen.c		\
		ft_create_list_max.c	\
		ft_create_node.c		\
		ft_delete.c				\
		ft_delete_node.c		\
		ft_down.c				\
		ft_erase_list.c			\
		ft_exit.c				\
		ft_get_backup.c			\
		ft_get_tty.c			\
		ft_pause.c				\
		ft_print_list.c			\
		ft_print_node.c			\
		ft_putchar_tty.c		\
		ft_reverse.c			\
		ft_select.c				\
		ft_send.c				\
		ft_send_key.c			\
		ft_set_param.c			\
		ft_set_term.c			\
		ft_up.c					\
		main.c


SRC := $(addprefix $(SRCDIR)/, $(SRC))

OBJ = $(SRC:$(SRCDIR)%.c=$(OBJDIR)%.o)

all: $(NAME)

$(NAME): $(OBJ)
	@echo "compilation du libft ..."
	@make -C $(LIBFTDIR) all
	@echo "finalisation de $(NAME) ..."
	@clang $(FLAGS) $(OBJ) -lncurses -I$(HEADLIBFT) \
		$(LIBFTDIR)/libft.a -o $(NAME)
	@echo "Complete !"

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@clang $(FLAGS) -I$(HEAD) -I$(HEADLIBFT) -c -o $@ $^
	@echo "compilation de $@ ..."

clean:
	@-rm -rf $(OBJ)
	@echo "suppresion des objets ..."

fclean: clean
	@-rm -rf $(NAME)
	@echo "suppression de $(NAME) ..."

re:
	@echo "reactualisation de $(NAME) : Full Mode ..."
	@make -C $(LIBFTDIR) fclean
	@make fclean
	@make all

relite:
	@echo "reactualisation de $(NAME) : Lite Mode ..."
	@make fclean
	@make all

.PHONY: all clean fclean re relite 
